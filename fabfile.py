from fabric.api import run, env, task, execute, local, put
import re, pprint, timeit

def checkOS():
    res = run('uname -a')
    return res.split()[0]

def checkCPU():
    res = run("cat /proc/cpuinfo|awk -F ': ' 'FNR==5 {print $2}'")
    return ' '.join(res.split())

def checkFreeMem():
    res = run("free -m|awk '/-\/+.*/ {print $4}'")
    return res + ' MB'

def checkFD():
    res = run("ulimit -n")
    return res

def checkDisk():
    res = run("df -m $HOME|awk 'END{print $(NF-2)}'")
    return res + ' MB'

def checkOracleJDK():
    res = run('java -version')
    cre = 'java version .*1.6.0.*Java\(TM\)'
    return len(re.findall(cre, res, re.S)) > 0

def checkFileWriteSpeed():
    res = run("export LANG=en_US.UTF-8; dd bs=1M count=128 if=/dev/zero of=test conv=fdatasync 2>&1|awk -F ', ' 'END{print $NF}'")
    return res

def checkNetworkSpeed():
    res = timeit.timeit("put('test')", setup='from fabric.api import put', number=3)
    return '{:.3}'.format(128*3/res) + " MB/s"

def detect():
    env.password = env.loginfo[env.host_string]
    host_report = {}
    host_report['OS Type'] = checkOS()
    host_report['CPU'] = checkCPU()
    host_report['Free Memory'] = checkFreeMem()
    host_report['Max File Descriptors'] = checkFD()
    host_report['Free Disk Space'] = checkDisk()
    host_report['Oracle JDK 1.6'] = checkOracleJDK()
    host_report['File Writing Speed'] = checkFileWriteSpeed()
    host_report['Network Transfer Speed'] = checkNetworkSpeed()
    return host_report

@task
def report(hostfile):
    local('dd bs=1M count=128 if=/dev/zero of=test conv=fdatasync')
    env.loginfo = {}
    with open(hostfile) as f:
        for line in f:
            (user, pwd) = line.strip().split(': ')
            env.loginfo[user] = pwd
            env.hosts.append(user)

    res = execute(detect)
    print('Test Report:')
    pprint.pprint(res)
    local('rm -f test')

@task
def sendmail():
    print('send mail...')
